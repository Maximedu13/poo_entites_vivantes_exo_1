package com.beweb.montpellier.poo.entites_vivantes.exercice.one.entities;

public class Eleve{

    private String nom;
    protected String prenom;

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    /**
     * Constructeur par défaut
     */
    public Eleve()
    {
        this.nom = new String("");
        this.prenom =""; //équivalent à new String();
    }

    /**
     * Affecter propriétes aux valeurs
     */
    public Eleve(String n, String prenom){
        this.nom = n;
        this.prenom = prenom;
    }

    public void afficheEleve(){
       System.out.println("Je m'appelle " + this.prenom + " " + this.nom);
    }
}