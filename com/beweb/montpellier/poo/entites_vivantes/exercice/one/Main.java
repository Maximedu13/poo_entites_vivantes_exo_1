package com.beweb.montpellier.poo.entites_vivantes.exercice.one;


import com.beweb.montpellier.poo.entites_vivantes.exercice.one.entities.Eleve;

public class Main{
    public static void main(String[] args) {
        Eleve e = new Eleve("lol", "lol");
        Eleve e2 = new Eleve("jean jacques", "l'homme en or");
        System.out.println(e2.getNom());
        e.afficheEleve();
        e2.afficheEleve();
    }
}
